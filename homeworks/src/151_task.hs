mylist (0,0,0,1) = 0
mylist (0,0,1,0) = mylist (0,0,0,1) + 1
mylist (0,1,0,0) = mylist (0,0,1,1) + 1
mylist (1,0,0,0) = mylist (0,1,1,1) + 1

mylist (a2,a3,a4,a5) =
    (varA2 + varA3 + varA4 + varA5) / (a2 + a3 + a4 + a5)
    where
    varA2 | a2 > 0 = a2 * mylist (a2-1,a3+1,a4+1,a5+1)
          | otherwise = 0
    varA3 | a3 > 0 = a3 * mylist (a2,a3-1,a4+1,a5+1)
          | otherwise = 0
    varA4 | a4 > 0 = a4 * mylist (a2,a3,a4-1,a5+1)
          | otherwise = 0
    varA5 | a5 > 0 = a5 * mylist (a2,a3,a4,a5-1)
          | otherwise = 0

task_151 = mylist (1,1,1,1)
main = print task_151
