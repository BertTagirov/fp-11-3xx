-- Term может быть переменной, лямбда от переменной, возвращающий терм, выполнение терма
data Term = Var String
          | Lambda String Term
          | Apply Term Term

-- выводим терм
instance Show Term where
    show (Var x) = x
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")"
    show (Apply term1 term2) = (show term1) ++ " " ++ (show term2)

-- переименовываем переменные, содержащиеся в списке, у которых совпадают имена
renameFreeVars :: [String] -> Term -> Term
renameFreeVars lst (Lambda x term) | elem x lst = Lambda x (renameFreeVars (filter (\el -> el /= x) lst) term)
                                   | otherwise = Lambda x (renameFreeVars lst term)
renameFreeVars lst (Apply t1 t2) = Apply (renameFreeVars lst t1) (renameFreeVars lst t2)
renameFreeVars lst (Var x) | elem x lst = Var (renameVar lst x 0)
                           | otherwise = Var x

-- получение уникальной строки, которой не в списке
renameVar :: [String] -> String -> Int -> String
renameVar lst x n | elem (x ++ (show n)) lst = renameVar lst x (n+1)
                  | otherwise = x ++ (show n)

-- Функция, заменяющая данную переменную на нужный терм
setVar :: String -> [String] -> Term -> Term -> Term
setVar var lst (Lambda x term) needTerm | x /= var = Lambda x (setVar var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVar var lst (Apply term1 term2) needTerm = Apply (setVar var lst term1 needTerm) (setVar var lst term2 needTerm)
setVar var lst (Var x) needTerm | x == var = renameFreeVars lst needTerm
                                          | otherwise = Var x

-- Делаем один шаг в нормализации терма
eval1 :: Term -> Term
eval1 (Apply (Lambda var res) t2) = setVar var [] res t2
eval1 (Apply (Apply term1 term2) t2) = Apply (eval1 (Apply term1 term2)) t2
eval1 term = term

id' = Lambda "x" (Var "x")
termList = [Apply id' (Apply id' (Lambda "z" (Apply id' (Var "z")))), Apply (Lambda "x" id') (Var "x"),
                 Apply (Apply (Lambda "x" (Lambda "y" (Apply (Var "x") (Var "y")))) (Lambda "z" (Var "z"))) (Lambda "u" (Lambda "v" (Var "v")))]

-- Делаем максимальное число шагов в нормализации терма
eval :: Term -> Term
eval (Apply (Lambda var res) t2) = eval (setVar var [] res t2)
eval (Apply (Apply term1 term2) t2) = eval (Apply (eval (Apply term1 term2)) t2)
eval term = term
