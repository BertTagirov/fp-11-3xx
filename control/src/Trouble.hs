{-==== ФАМИЛИЯ ИМЯ, НОМЕР ГРУППЫ ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if snd (getMaxRatio xs) == getMin xs
            then [getMaxRatio xs]
            else xs
getMaxRatio :: [(Integer, Integer)] -> (Integer, Integer)
getMaxRatio (x:[]) = x
getMaxRatio (x:xs) = if ((fst x) 'quot'  (snd x)) > (fst (getMaxRatio xs)) 'quot' (snd (getMaxRatio xs))
            then x
            else getMaxRatio xs

getMin :: [(Integer, Integer)] -> Integer
getMin (x:[]) = snd x
getMin (x:xs) | snd x < getMin xs = snd x
              | otherwise = getMin xs

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo a) : as) = (orbiters as) ++ (getMyOrbiters a)
orbiters ((Rocket _  a) : as) = (orbiters as) ++ (orbiters a)

getMyOrbiters :: [Load a] -> [Load a]
getMyOrbiters [] = []
getMyOrbiters ((Orbiter a): as) = (Orbiter a) : (getMyOrbiters as)
getMyOrbiters ((Probe a): as) = getMyOrbiters as


{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ((Warp _ ) : as ) = "Kirk" : (finalFrontier as)
finalFrontier ((BeamUp _) : as) = "Kirk" : (finalFrontier as)
finalFrontier ((IsDead _ ) : as) = "McCoy" : (finalFrontier as)
finalFrontier ((LiveLongAndProsper _) : as) = "Spock" : (finalFrontier as)
finalFrontier ((Fascinating _) : as) = "Spock" : (finalFrontier as)
